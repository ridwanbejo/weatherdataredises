var redis = require("redis");
var dateFormat = require('dateformat');
var elasticsearch = require('elasticsearch');

var client = new elasticsearch.Client({
  host: 'localhost:9200',
  log: 'trace'
});

var subscriber = redis.createClient();

subscriber.on("message", function (channel, message) {
	console.log("Message: " + message + " on channel: " + channel + " is arrive!");

	var payload = JSON.parse(message);

	// store document to elasticsearch
	client.index({
	  index: 'myiot',
	  type: 'weather',
	  body: {
			"device_id": payload.device_id,
			"message_id": payload.message_id, 
			"humidity": payload.humidity, 
			"temperature_in_celsius": payload.temperature_in_celsius, 
			"createdAt": dateFormat(new Date(), "yyyy-mm-dd hh:mm:ss")
		}
	}, function (error, response) {

		console.log(error);
		console.log(response);
	});
});

subscriber.subscribe("weatherData");

