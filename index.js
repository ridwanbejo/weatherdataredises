var redis = require("redis");
var publisher = redis.createClient();
const uuidv4 = require('uuid/v4');

var device_id = "asus-12345";

function sendData()
{
	var message = {
		"device_id": device_id,
		"message_id": uuidv4(), 
		"humidity": Math.random() * 100, 
		"temperature_in_celsius": Math.random() * 100
	};

	publisher.publish("weatherData", JSON.stringify(message), function(){
		setTimeout(function(){
			sendData();
		}, 1000);
	});
}

sendData();